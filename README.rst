SuPyGirls
=========
 |SupyGirls|

Desenvolvimento da plataforma SuperPython para o projeto supygirls.

 |docs| |python| |license| |gitlab|


:Author:  Carlo E. T. Oliveira
:Version: 20.07
:Affiliation: Universidade Federal do Rio de Janeiro
:License: GNU General Public License v3 or later (GPLv3+)
:Homepage: http://supygirls.activufrj.nce.ufrj.br
:Changelog: `CHANGELOG <CHANGELOG.rst>`_

SuperPython Section
-------------------

SuperPython is a WEB IDE designed to teach Python. It is specially target to teach children in early scholar age.

The SuPyGirls implementation uses a preset collection of projects and modules.
Persistence is done saving all code to git.

-------

Laboratório de Automação de Sistemas Educacionais
-------------------------------------------------

**Copyright © Carlo Olivera**

LABASE_ - NCE_ - UFRJ_

|LABASE|

.. _LABASE: http://labase.activufrj.nce.ufrj.br
.. _NCE: http://nce.ufrj.br
.. _UFRJ: http://www.ufrj.br

.. |gitlab| image:: https://img.shields.io/badge/gitlab-project-blue
   :target: https://gitlab.com/cetoli/SuPyGirls


.. |LABASE| image:: https://cetoli.gitlab.io/spyms/image/labase-logo-8.png
   :target: http://labase.activufrj.nce.ufrj.br
   :alt: LABASE

.. |SupyGirls| image:: https://gitlab.com/cetoli/SuPyGirls/-/raw/master/docs/source/_static/supygirls_logo.png
   :target: http://supygirls.activufrj.nce.ufrj.br
   :alt: SUPYGIRLS

.. |python| image:: https://img.shields.io/badge/python-3.8-blue
   :target: https://www.python.org/downloads/release/python-383/

.. |docs| image:: https://img.shields.io/readthedocs/supygirls
   :target: https://supygirls.readthedocs.io/en/latest/index.html

.. |license| image:: https://img.shields.io/github/license/cetoli/_spy
   :target: https://gitlab.com/cetoli/SuPyGirls/blob/master/LICENSE
